#  VizuaMatix Assessment

This is the answer for the assessment that given by VizuaMatix for the position SE. This assessment is on develop small calculator to calculate the price that customer purchased. 

According to the requirement of the assessment, it says to build a simple application that include CRUD operation. It must contain front end and back end in separate module. Front end communicate with back end using REST API endpoints.

According to above requirement I have built spring boot application to handle back end logic, react app to handle front end logic, JUnit has used for testing purposes. Addition to requirement I have built swagger ui, docker file and docker-compose file.

# Architectural Diagram
![Update Item](documentation/assignment-diagram.jpg)

# Requirement developed
This application is a simple shopping cart. User can add items, update items, delete items, view items, can add items to cart etc.
### Add New Item
![Add Item](documentation/Screenshots/AddItem.png?raw=true)

### Update Item details
![Update Item](documentation/Screenshots/updateItem.png)

### Item List
![Home](documentation/Screenshots/index.png)

### Shopping Cart
![Cart](documentation/Screenshots/cart.png)

### Detail of Items
![Detail](documentation/Screenshots/viewMore.png)

# Set up application in your machine

## Set up back end API
To set up back end api these pre requirements are needed to install in your machine

 - JAVA 8
 - My SQL
 - Maven

If you have above pre requirements in your machine, then follow these steps to setup back end api. 

 1. Navigate to the "`api`" folder
 2. If you want to change the database properties like database name, database user name, database password and others, you have to go to the file "`application.properties`". This file is available at "`api/src/java/main/resources`" folder. There you can set up the database as you wanted. If not keep as it is.
 3. Then open terminal and give the command "`mvn clean instal`l" to install maven dependencies and create jar file under target folder.
 4. Then navigate to the target folder using terminal. It will available at "api/target".
 5. Give the command `java -jar assessment-api.jar` to run application.
 6. If there are no compilation issue happen, the server will run on port "8080" (If you want to change the port of the api then add the `server.port` property in application.properties file. eg :- `server.port=8081`)

## Set up front end application
To set up front end application these pre requirements are needed to install in your machine

 - Node js (v>=10.0)

If you have above pre requirements in your machine, then follow these steps.

 1. Navigate to the "`View`" folder
 2. Give command "`npm install`" to install packages
 3. After installation of packages run "`npm start`".
 4. Then client app will automatically open in web browser under port 3000.

 
 