import axios from 'axios';

export class RestController {
    get(path) {
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    "Content-Type": "application/json"
                }
            }
            axios.get(path, config).then(response => {
                resolve(response);
            }).catch(err => {
                reject(err);
            })
        })
    }

    post(path, data) {
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    "Content-Type": "application/json"
                }
            }
            axios.post(path, data, config).then(response => {
                resolve(response);
            }).catch(err => {
                reject(err);
            })
        })
    }

    put(path, data) {
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    "Content-Type": "application/json"
                }
            }
            axios.put(path, data, config).then(response => {
                resolve(response);
            }).catch(err => {
                reject(err);
            })
        })
    }

    delete(path) {
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    "Content-Type": "application/json"
                }
            }
            axios.delete(path, config).then(response => {
                resolve(response);
            }).catch(err => {
                reject(err);
            })
        })
    }
}