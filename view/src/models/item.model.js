export class Item {
    id;
    itemName;
    noOfUnitsInCartoon;
    priceOFSingleCartoon;
    imageUrl;
    increasedPrecentage;
    discountPrecentage;
    minCartoonAmountToDiscount;
}