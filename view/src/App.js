import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Lines } from 'react-preloaders';
import { Nav, Navbar } from 'react-bootstrap';

import { ItemManager } from './components/itemManager.component';
import { Index } from './components/index.component';
import { Help } from './components/help.component';

class App extends Component {
  render() {
    return(
      <Router>
        <div className="col-sm-12">
          <Navbar collapseOnSelect bg="dark" expand="lg" fixed="top" variant="dark">
            <Navbar.Brand href="/">Sale Calculator</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/manager">Item Manager</Nav.Link>
                <Nav.Link href="/help">Help</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Switch>
            <Route exact path="/" component={Index}/>
            <Route exact path="/manager" component={ItemManager}/>
            <Route exact path="/help" component={Help}/>
          </Switch>
        </div>
        <Lines color={'#f7f7f7'}/>
      </Router>
    );
  }
}

export default App;
