import React, { Component } from 'react';
import { RestController } from '../Services/restController';
import { ConstValues } from '../properties/constValues';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Item } from '../models/item.model';
import { Purchased } from '../models/purchased.model';

import { ItemCard } from './utils/itemCard';
import { Cart } from './utils/cart';

export class Index extends Component {
    constructor(props) {
        super(props);
        this.loadItems = this.loadItems.bind(this);
        this.renderItems = this.renderItems.bind(this);
        this.getPurchasedAmount = this.getPurchasedAmount.bind(this);
        this.removePurchasedItem = this.removePurchasedItem.bind(this);
        this.calculateTotalPrice = this.calculateTotalPrice.bind(this);
        this.state = {
            items: [],
            totalPrice: 0,
            purchasedItems: [],
            cart: new Cart(),
            restController: new RestController()
        }
    }

    componentDidMount() {
        this.loadItems();
    }

    render() {
        return(
            <div style={{"paddingTop": "85px"}}>
                <div className="row">
                    <div className="col-sm-8 row">
                        {this.renderItems()}
                    </div>
                    <div className="col-sm-4">
                        <Cart purchasedItems={this.state.purchasedItems} remomvePurchasedItem={this.removePurchasedItem} totalPrice={this.state.totalPrice}></Cart>
                    </div>
                </div>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </div>
        );
    }

    loadItems() {
        this.state.restController.get(ConstValues.itemService).then(response => {
            let itemArray = [];
            response.data.forEach(item => {
                let newItem = new Item();
                newItem.imageUrl = item.imageUrl;
                newItem.id = item.id;
                newItem.increasedPrecentage = item.increasedPrecentage;
                newItem.itemName = item.itemName;
                newItem.minCartoonAmountToDiscount = item.minCartoonAmountToDiscount;
                newItem.noOfUnitsInCartoon = item.noOfUnitsInCartoon;
                newItem.priceOFSingleCartoon = item.priceOFSingleCartoon;
                newItem.discountPrecentage = item.discountPrecentage;
                itemArray.push(newItem);
            })
            this.setState({
                items: itemArray
            })
        }).catch(err => {
            toast.warning("There is an internal error, please try later");
        })
    }

    getPurchasedAmount(newPurchasedItem) {
        let alreadyPurchased = false;
        let purchasedAmount = 0;
        this.state.purchasedItems.map(purchaseItem => {
            if(purchaseItem.id == newPurchasedItem.itemId) {
                alreadyPurchased = true;
                purchasedAmount = (purchaseItem.cartonAmount * purchaseItem.itemsInCaton) + purchaseItem.singleAmount + newPurchasedItem.amount;
            } 
        })
        if(alreadyPurchased == false) {
            purchasedAmount = newPurchasedItem.amount;
        }
        this.state.restController.get(ConstValues.itemService + "/calculate_price/single/" + newPurchasedItem.itemId + "/" + purchasedAmount).then(response => {
            let purchasedItemArray = this.state.purchasedItems;
            if(alreadyPurchased) {
                let index = this.state.purchasedItems.findIndex(item => item.id === newPurchasedItem.itemId);
                purchasedItemArray[index].cartonAmount = parseInt(purchasedAmount / newPurchasedItem.amountInCarton);
                purchasedItemArray[index].singleAmount = purchasedAmount % newPurchasedItem.amountInCarton;
                purchasedItemArray[index].price = response.data.price;
            } else {
                let newItemPurchase = new Purchased();
                newItemPurchase.id = newPurchasedItem.itemId;
                newItemPurchase.itemImage = newPurchasedItem.image;
                newItemPurchase.itemName = newPurchasedItem.itemName;
                newItemPurchase.itemsInCaton = newPurchasedItem.amountInCarton;
                newItemPurchase.cartonAmount = parseInt(newPurchasedItem.amount / newPurchasedItem.amountInCarton);
                newItemPurchase.singleAmount = newPurchasedItem.amount % newPurchasedItem.amountInCarton;
                newItemPurchase.price = response.data.price;
                purchasedItemArray.push(newItemPurchase);
            }
            this.setState({
                purchasedItems: purchasedItemArray
            })
            this.calculateTotalPrice(this.state.purchasedItems);
            toast.success("Successfully added " + newPurchasedItem.itemName + " to cart");
        }).catch(err => {
            toast.warning("Not added item successfully to cart, please try again");
        })
    }

    removePurchasedItem(itemId) {
        let purchasedItems = this.state.purchasedItems;
        purchasedItems = purchasedItems.filter(item => item.id != itemId);
        this.calculateTotalPrice(purchasedItems);
        this.setState({
            purchasedItems: purchasedItems
        })
    }

    calculateTotalPrice(itemArray) {
        let total = 0;
        itemArray.map(item => {
            total = total + item.price;
        })
        this.setState({
            totalPrice: total
        })
    }

    renderItems() {
        return this.state.items.map(item => {
            return <ItemCard key={item.id} item={item} onPurchased={this.getPurchasedAmount} />;
        })
    }
}