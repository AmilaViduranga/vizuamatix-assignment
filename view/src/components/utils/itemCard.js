import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';

export class ItemCard extends Component{
    
    constructor(props) {
        super(props);
        this.onChangeCartonAmount = this.onChangeCartonAmount.bind(this);
        this.onChangeSingleAmount = this.onChangeSingleAmount.bind(this);
        this.submitToCart = this.submitToCart.bind(this);
        this.loadModal = this.loadModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.state = {
            cartonAmount: "",
            singleUnitAmount: "",
            imageUrl: "https://image.shutterstock.com/image-vector/no-image-available-sign-absence-260nw-373243873.jpg",
            show: false,
            handleClose: false
        }
    }

    onChangeCartonAmount(event) {
        let cartonAmount = event.target.value;
        this.setState({
            cartonAmount: cartonAmount
        })
    }

    onChangeSingleAmount(event) {
        let singleAmount = event.target.value;
        this.setState({
            singleUnitAmount: singleAmount
        })
    }

    loadModal() {
        this.setState({
            show: true
        })
    }

    closeModal() {
        this.setState({
            show: false
        })
    }

    submitToCart() {
        if(this.state.cartonAmount == "" || this.state.cartonAmount < 0) {
            this.setState({
                cartonAmount: 0
            })
        }
        if(this.state.singleUnitAmount == "" || this.state.singleUnitAmount < 0) {
            this.setState({
                singleUnitAmount: 0
            })
        }
        let totalAmount = parseInt(this.props.item.noOfUnitsInCartoon * this.state.cartonAmount) + parseInt(this.state.singleUnitAmount * 1);
        this.props.onPurchased({
            itemId: this.props.item.id,
            amount: totalAmount,
            amountInCarton: this.props.item.noOfUnitsInCartoon,
            itemName: this.props.item.itemName,
            image: this.props.item.imageUrl
        })
    }

    render() {
        return(
            <div className="col-sm-3" style={{"marginBottom": "5px"}}>
                <div className="card">
                    <img className="card-img-top img-fluid" src={this.props.item.imageUrl || this.state.imageUrl} alt="Card image" style={{"width": "300px", "height": "230px"}}/>
                    <div className="card-body">
                        <h3>{this.props.item.itemName}</h3>
                        <small>
                            <b>Carton price :- Rs {this.props.item.priceOFSingleCartoon.toFixed(2)}</b><br/>
                            <b>Item count in carton :- {this.props.item.noOfUnitsInCartoon}</b>
                        </small>
                        <div>
                            <input className="form-control" type="number" min="0" value={this.state.cartonAmount} style={{"marginBottom": "15px"}} placeholder="Carton amount" onChange={this.onChangeCartonAmount}/>
                            <input className="form-control" type="number" min="0" value={this.state.singleUnitAmount} style={{"marginBottom": "15px"}} placeholder="Single amount" onChange={this.onChangeSingleAmount}/>
                            <div className="btn-group float-right">
                                <button className="btn btn-primary" onClick={this.submitToCart}>Add to cart</button>
                                <button className="btn btn-warning" onClick={this.loadModal}>View More</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.show} onHide={this.closeModal} animation={false} style={{"paddingTop": "45px"}}>
                    <Modal.Header closeButton>
                        <Modal.Title>Item Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            <div className="col-sm-4">
                                <img className="card-img-top img-fluid" src={this.props.item.imageUrl || this.state.imageUrl} alt="Card image" style={{"width": "200px", "height": "200px"}}/>
                            </div>
                            <div className="col-sm-8">
                                <h3 style={{"textAlign": "center"}}>{this.props.item.itemName}</h3>
                                <ul>
                                    <li>Single carton price is Rs {this.props.item.priceOFSingleCartoon}</li>
                                    <li>Items in a single carton is {this.props.item.noOfUnitsInCartoon}</li>
                                    <li>Minimum items needed to purchase to get discount is {this.props.item.minCartoonAmountToDiscount}</li>
                                    <li>Discount presentage that apply is {this.props.item.discountPrecentage * 100}%</li>
                                    <li>Additional charge presentage if you purchase single item is {this.props.item.increasedPrecentage * 100} %</li>
                                </ul>
                                <div>
                                    <input className="form-control" type="number" min="0" value={this.state.cartonAmount} style={{"marginBottom": "15px"}} placeholder="Carton amount" onChange={this.onChangeCartonAmount}/>
                                    <input className="form-control" type="number" min="0" value={this.state.singleUnitAmount} style={{"marginBottom": "15px"}} placeholder="Single amount" onChange={this.onChangeSingleAmount}/>
                                    <div className="btn-group float-right">
                                        <button className="btn btn-primary" onClick={this.submitToCart}>Add to cart</button>
                                        <button className="btn btn-warning" onClick={this.loadModal}>View More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
} 