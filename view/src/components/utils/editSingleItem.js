import React, { Component } from 'react';
import { Create } from '../create.component';
import { ToastContainer, toast } from 'react-toastify';
import { ConstValues } from '../../properties/constValues';

export class EditSingleItem extends Create {
    constructor(props) {
        super(props);
        this.onUpdate = this.onUpdate.bind(this);

    }
    
    componentDidMount() {
        this.setState({
            itemName: this.props.item.itemName,
            noOfUnitsInCartoon: this.props.item.noOfUnitsInCartoon,
            priceOFSingleCartoon: this.props.item.priceOFSingleCartoon,
            imageUrl: this.props.item.imageUrl,
            increasedPrecentage: this.props.item.increasedPrecentage,
            discountPrecentage: this.props.item.discountPrecentage,
            minCartoonAmountToDiscount: this.props.item.minCartoonAmountToDiscount
        })
    }

    onUpdate(event) {
        if(this.state.itemName !== "" && this.state.itemName != undefined
        && this.state.minCartoonAmountToDiscount !== undefined && this.state.minCartoonAmountToDiscount >=0
        && this.state.noOfUnitsInCartoon !== undefined && this.state.noOfUnitsInCartoon >=0 
        && this.state.priceOFSingleCartoon !== undefined && this.state.priceOFSingleCartoon >=0
        && this.state.increasedPrecentage !== undefined && this.state.increasedPrecentage >=0 
        && this.state.discountPrecentage !== undefined && this.state.discountPrecentage >= 0) {
            this.state.restController.post(ConstValues.itemService, {
                id: this.props.item.id,
                itemName: this.state.itemName,
                noOfUnitsInCartoon: this.state.noOfUnitsInCartoon,
                priceOFSingleCartoon: this.state.priceOFSingleCartoon,
                imageUrl: this.state.imageUrl,
                increasedPrecentage: parseFloat((this.state.increasedPrecentage/100).toFixed(2)),
                discountPrecentage: parseFloat((this.state.discountPrecentage/100).toFixed(2)),
                minCartoonAmountToDiscount: this.state.minCartoonAmountToDiscount
            }).then(response => {
                toast.success("Successfully update item");
                this.props.loadItems();
                this.setState({
                    itemName: "",
                    noOfUnitsInCartoon: 0,
                    priceOFSingleCartoon: 0,
                    imageUrl: "https://image.shutterstock.com/image-vector/no-image-available-sign-absence-260nw-373243873.jpg",
                    increasedPrecentage: 0,
                    discountPrecentage: 0,
                    minCartoonAmountToDiscount: 0
                })
            }).catch(err => {
                toast.warning("Error, item is not updated properly, please try again");
            })
        } else {
            toast.warn("Validation error, please check required fields");
            this.onChangeItemName({target: { value: this.state.itemName}});
            this.onChangeDiscountPresentage({target: { value: this.state.discountPrecentage}});
            this.onChangeSingleCartonPrice({target: {value: this.state.priceOFSingleCartoon}});
            this.onChangeIncreasedPresentage({target: {value: this.state.increasedPrecentage}});
            this.onChangeMinAmountDiscount({target: {value: this.state.minCartoonAmountToDiscount}});
            this.onChangeUnitsInSingleCarton({target: {value: this.state.noOfUnitsInCartoon}});
        }
    }

    render() {
        return(
            <div className="col-sm-12">
                <div className="card">
                    <div className="card-header">
                        <h3>Update Item</h3>
                    </div>
                    <div className="card-body row container">
                        <div className="form-group">
                            <img  className="img-fluid" src={this.state.imageUrl} alt="item-image" style={{"borderStyle": "groove", "width": "400px", "height": "350px", "cursor": "pointer"}} onClick={this.loadImageFileContainer}/>
                            <input id="imageFileContainer" type="file" style={{"display":"none"}} ref={this.fileUpload} onChange={this.onChangeImage}/>
                        </div>
                    </div>
                    <div className="card-body row" style={{"padding": "10px"}}>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="itemName">Item Name</label>
                                <input type="text" className="form-control" id="itemName" aria-describedby="itemNameHelp" placeholder="give item name eg:- soap" value={this.state.itemName} onChange={this.onChangeItemName} onClick={this.onClickItemName}/>
                                <small id="validationItemName">{this.state.validateItemName == false ? <span style={{"color": "red"}}>{this.state.validateMessageItemName}</span>:<span></span>}</small>
                                <small id="itemNameHelp" className="form-text text-muted">The name of the item that you needed to display</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="unitsCarton">Units in single carton</label>
                                <input type="number" className="form-control" id="unitsCarton" aria-describedby="unitsCartonHelp" placeholder="give units in single carton eg :- 4" value={this.state.noOfUnitsInCartoon} onChange={this.onChangeUnitsInSingleCarton} min="0"/>
                                <small id="validationUnitCarton">{this.state.validateNoOFUnitsInCarton == false ? <span style={{"color": "red"}}>{this.state.validateMessageUnitsInSingleCarton}</span>:<span></span>}</small>
                                <small id="unitsCartonHelp" className="form-text text-muted">Give units in a single carton, If there is no carton available please give value as 0</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="cartonPrice">Single carton price</label>
                                <input type="number" className="form-control" id="cartonPrice" aria-describedby="cartonPriceHelp" placeholder="give price of carton eg:- Rs 240.30" value={this.state.priceOFSingleCartoon} onChange={this.onChangeSingleCartonPrice} min="0"/>
                                <small id="validationSingleCartonPrice">{this.state.validatePriceOfSingleCarton == false ? <span style={{"color": "red"}}>{this.state.validateMessageForSingleCartonPrice}</span>:<span></span>}</small>
                                <small id="cartonPriceHelp" className="form-text text-muted">Single carton price</small>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="minAmount">Minimum amount to discount</label>
                                <input type="number" className="form-control" id="minAmount" aria-describedby="minAmountHelp" placeholder="give minimum amount of caron that apply discount eg:- 3" value={this.state.minCartoonAmountToDiscount} onChange={this.onChangeMinAmountDiscount} min="0"/>
                                <small id="validationMinAmount">{this.state.validateMinAmountToDiscount == false ? <span style={{"color": "red"}}>{this.state.validateMessageForMinCartonAmountDiscount}</span>:<span></span>}</small>
                                <small id="minAmountHelp" className="form-text text-muted">Give the minimum carton amount that need to apply discount</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="increasedPresentage">Increased Presentage</label>
                                <input type="number" className="form-control" id="increasedPresentage" aria-describedby="increasedPresentageHelp" placeholder="give presentage that apply single item purchased eg:- 30" min="0" max="100" value={this.state.increasedPrecentage} onChange={this.onChangeIncreasedPresentage} min="0"/>
                                <small id="validationIncreasedPresentage">{this.state.validateIncreasedPresentage == false ? <span style={{"color": "red"}}>{this.state.validateMessageForIncreasedPresentage}</span>:<span></span>}</small>
                                <small id="increasedPresentageHelp" className="form-text text-muted">Increased Presentage that apply for single item purchased, Please give 0 if you not going to apply any presentage</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="discountPresentage">Discount Presentage</label>
                                <input type="number" className="form-control" id="discountPresentage" aria-describedby="discountPresentageHelp" placeholder="give discount presentage that apply to purchase carton eg:- 20" value={this.state.discountPrecentage} onChange={this.onChangeDiscountPresentage} min="0"/>
                                <small id="validationDiscountPresentage">{this.state.validateDiscountPresentage == false ? <span style={{"color": "red"}}>{this.state.validateMessageForDiscountPresentage}</span>:<span></span>}</small>
                                <small id="discountPresentageHelp" className="form-text text-muted">Discount apply if customer purchase more than minimum carton amount</small>
                            </div>
                            <button className="btn btn-success pull-right" style={{"margin": "10px"}} onClick={this.onUpdate}>Update Item</button>
                        </div>
                    </div>
                </div>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </div>
        );
    }
}