import React, { Component } from 'react';
import { SinglePurchasedItem } from './singlePurchasedItem';

export class Cart extends Component {
    constructor(props) {
        super(props);
        this.removeItem = this.removeItem.bind(this);
    }

    removeItem(itemId) {
        this.props.remomvePurchasedItem(itemId);
    }

    render() {
        return(
            <div className="itemCard">
                <h3>Shopping Cart</h3>
                <table className="table table-responsive">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Item</th>
                            <th scope="col">Carton Amount</th>
                            <th scope="col">Single Amount</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.purchasedItems.map(item => {
                            return(
                                <SinglePurchasedItem key={item.id} item={item} removeItem={this.removeItem}></SinglePurchasedItem>
                            );
                        })}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <b>Total Amount</b>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Rs {this.props.totalPrice.toFixed(2)}</td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        );
    }
}