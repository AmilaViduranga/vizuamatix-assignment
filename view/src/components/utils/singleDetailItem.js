import React, { Component } from 'react';

export class SingleDetailItem extends Component {
    constructor(props) {
        super(props);
        this.deleteItem = this.deleteItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
    }

    deleteItem() {
        this.props.removeItem(this.props.item.id)
    }

    updateItem() {
        this.props.updateItem(this.props.item);
    }
    
    render() {
        return(
            <tr>
                <td>
                    <img src={this.props.item.imageUrl} style={{"width": "50px", "height": "50px", "borderRadius": "50%"}}/>
                </td>
                <td>{this.props.item.itemName}</td>
                <td>{this.props.item.noOfUnitsInCartoon}</td>
                <td>Rs {this.props.item.priceOFSingleCartoon.toFixed(2)}</td>
                <td>{this.props.item.minCartoonAmountToDiscount}</td>
                <td>{parseInt(this.props.item.increasedPrecentage * 100)}%</td>
                <td>{parseInt(this.props.item.discountPrecentage * 100)}%</td>
                <td className="btn-group">
                    <button className="btn btn-success" onClick={this.updateItem}>Update</button>
                    <button className="btn btn-danger" onClick={this.deleteItem}>Delete</button>
                </td>
            </tr>
        );
    }
}