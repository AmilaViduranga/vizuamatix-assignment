import React, { Component } from 'react';

export class SinglePurchasedItem extends Component{
    constructor(props) {
        super(props);
        this.deleteItem = this.deleteItem.bind(this);
    }

    deleteItem() {
        this.props.removeItem(this.props.item.id)
    }
    
    render() {
        return(
            <tr>
                <td>
                    <img src={this.props.item.itemImage} style={{"width": "50px", "height": "50px", "borderRadius": "50%"}}/>
                </td>
                <td>{this.props.item.itemName}</td>
                <td>{this.props.item.cartonAmount}</td>
                <td>{this.props.item.singleAmount}</td>
                <td>Rs {this.props.item.price.toFixed(2)}</td>
                <td>
                    <button className="btn btn-danger" onClick={this.deleteItem}>-</button>
                </td>
            </tr>
        );
    }
}