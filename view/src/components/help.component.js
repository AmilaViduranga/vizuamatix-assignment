import React, { Component } from 'react';

export class Help extends Component {
    render() {
        return(
            <div style={{"paddingTop": "85px"}}>
                <section id="introduction">
                    <h3 style={{"color": "blue"}}>Introduction</h3>
                    <p>In this section I will describe about the application in business point of view. No technical explanation included. Only about 
                        what I built, what user of this system can perform by using this system.
                    </p>
                    <p>This is the assessment that develop for software engineering possition at vizuamatix. They have given me a task to build
                        a simple crud opperation using springboot, hibernate and react. Here is the mail that I got from hr team.
                    </p>
                    <img className="img-fluid" width="750" height="600" src={process.env.PUBLIC_URL + "diagrams/email.png"}/>
                    <p>After reading about this email, I plan a small crud application called <b>Sale Calculator</b> application.The actions that we can perform
                    by this application are listed bellow</p>
                    <ul>
                        <li>User can insert, update, delete an item</li>
                        <li>User can define the discount prices, minimum amount that has to purchase to apply discount, charging amount if the user buy single item</li>
                        <li>User can view more details about item before it add to cart</li>
                        <li>User can properly manage his cart (vie the cart, add item to cart, remove item from cart)</li>
                    </ul>
                </section>
                <section id="howToSetup">
                    <h3 style={{"color": "blue"}}>Setup Application in your machine</h3>
                    <p>Gitlab url is <a href="https://gitlab.com/AmilaViduranga/vizuamatix-assignment" target="_blank">here</a></p>
                    <video width="750" height="600" controls>
                        <source src={process.env.PUBLIC_URL + "diagrams/application.mp4"} type="video/mp4"/>
                        Your browser does not support the video tag.
                    </video>
                </section>
                <section id="architecturalDiagram" style={{"marginTop": "20px"}}>
                    <h3 style={{"color": "blue"}}>Technical background</h3>
                    <p>In this section I will discuss about architectural diagram, technologies that I used for back end, technologies that I used for front end, unit testcases that I have written</p>
                    <img className="img-fluid" width="750" height="600" src={process.env.PUBLIC_URL + "diagrams/architecture.jpg"}/>
                    <p>In this diagram entire client part in written by using <b>React JS</b>. Controller, Service Layer, Models, Repository Class are the components writtern by <b>Spring Boot</b>.
                    The database is <b>My SQL</b>. I have used <b>Hibernate</b> as ORM tool. Clent will consume the API using the endpoints that are registered at controller.</p>
                    <img className="img-fluid container" width="850" height="750" src={process.env.PUBLIC_URL + "diagrams/swager.png"}/>
                    <p>All the JUnit test cases result that I written is mention bellow</p>
                    <img className="img-fluid" width="450" height="350" src={process.env.PUBLIC_URL + "diagrams/junit.png"}/>
                </section>
            </div>
        );
    }
}