import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { ConstValues } from '../properties/constValues';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { RestController } from '../Services/restController';
import { Item } from '../models/item.model';

import { Create } from './create.component';
import { Edit } from './edit.component';
import { EditSingleItem } from './utils/editSingleItem';

export class ItemManager extends Component {
    constructor(props) {
        super(props);
        this.loadItems = this.loadItems.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.state =  {
            items: [],
            show: false,
            updatableItem: null,
            restController: new RestController()
        }
    }

    componentDidMount() {
        this.loadItems();
    }

    loadItems() {
        this.state.restController.get(ConstValues.itemService).then(response => {
            let itemArray = [];
            response.data.forEach(item => {
                let newItem = new Item();
                newItem.imageUrl = item.imageUrl;
                newItem.id = item.id;
                newItem.increasedPrecentage = item.increasedPrecentage;
                newItem.itemName = item.itemName;
                newItem.minCartoonAmountToDiscount = item.minCartoonAmountToDiscount;
                newItem.noOfUnitsInCartoon = item.noOfUnitsInCartoon;
                newItem.priceOFSingleCartoon = item.priceOFSingleCartoon;
                newItem.discountPrecentage = item.discountPrecentage;
                itemArray.push(newItem);
            })
            this.closeModal();
            this.setState({
                items: itemArray
            })
        }).catch(err => {
            toast.warning("There is an internal error, please try later");
        })
    }

    removeItem(itemId) {
        let isConfirmed = window.confirm("Are you sure about this action?");
        if(isConfirmed) {
            this.state.restController.delete(ConstValues.itemService + "/" + itemId).then(response => {
                if(response.status == 200 && response.data == "FORBIDDEN") {
                    toast.success("Successfully delete Item");
                    this.loadItems();
                } else {
                    toast.warning("Your delete process is not successfully hapen, please try again");
                }
            }).catch(err => {
                toast.warning("Your delete process is not successfully hapen, please try again");
            })
        }
    }

    updateItem(item) {
        this.setState({
            show: true,
            updatableItem: item
        })
    }

    closeModal() {
        this.setState({
            show: false
        })
    }

    render() {
        return(
            <div style={{"paddingTop": "85px"}} className="row">
                <div className="col-sm-6">
                    <Create loadItems={this.loadItems}/>
                </div>
                <div className="col-sm-6">
                    <Edit items={this.state.items} removeItem={this.removeItem} updateItem={this.updateItem}/>
                </div>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <Modal show={this.state.show} onHide={this.closeModal} animation={false} style={{"paddingTop": "45px"}}>
                    <Modal.Header closeButton>
                        <Modal.Title>Item Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditSingleItem loadItems={this.loadItems} item={this.state.updatableItem}/>
                    </Modal.Body>
                </Modal>    
            </div>
        );
    }
}