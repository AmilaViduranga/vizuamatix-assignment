import React, { Component } from 'react';
import { SingleDetailItem } from './utils/singleDetailItem';

export class Edit extends Component {
    constructor(props) {
        super(props);
        this.removeItem = this.removeItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
    }

    removeItem(itemId) {
        this.props.removeItem(itemId);
    }

    updateItem(item) {
        this.props.updateItem(item);
    }

    render() {
        return(
            <div>
                <table className="table table-responsive">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Item Name</th>
                            <th scope="col">Units In A Carton</th>
                            <th scope="col">Single Carton Price</th>
                            <th scope="col">Minimum Amount To Discount</th>
                            <th scope="col">Increased Presentage</th>
                            <th scope="col">Discount Precentage</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.items.map(item => {
                            return(
                                <SingleDetailItem key={item.id} item={item} removeItem={this.removeItem} updateItem={this.updateItem}/>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}